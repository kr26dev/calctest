import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CalcTest {

    private static WebDriver webDriver;

    private static final String url = "http://46.174.255.4:8082/vaadincalc/";

    private static final String a_xpath = "//div[@id='vaadincalc-2022521208']/div/div[2]/div[3]/div/div[2]/div/div/div/div/input";

    private static final String b_xpath = "//div[@id='vaadincalc-2022521208']/div/div[2]/div[3]/div/div[2]/div/div/div[3]/div/input";

    private static final String c_xpath = "//div[@id='vaadincalc-2022521208']/div/div[2]/div[3]/div/div[2]/div/div/div[5]/div/input";

    private static final String button_xpath = "//div[@id='vaadincalc-2022521208']/div/div[2]/div[3]/div/div[2]/div/div/div[7]/div";

    private static final String message_xpath = "//div[@id='vaadincalc-2022521208']/div/div[2]/div[3]/div/div[2]/div/div/div/div/div[2]";



    public static void main(String[] args) throws Exception {

        System.setProperty("webdriver.gecko.driver","C:\\Gecko\\geckodriver.exe");

        // Тест на корректное вычисление
        successTest();

        // Тест деления на 0
        zeroTest();

        // Тест на ввод текстовых символов
        incorrectTextTest();

        // Тест на ввод спецсимволов
        incorrectSpecTest();

    }


    private static boolean failureTestExecute(String a, String b) throws Exception {

        webDriver = new FirefoxDriver();

        webDriver.get(url);

        WebElement expectation = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(button_xpath)));

        WebElement aNum = webDriver.findElement(By.xpath(a_xpath));

        WebElement bNum = webDriver.findElement(By.xpath(b_xpath));

        WebElement button = webDriver.findElement(By.xpath(button_xpath));

        WebElement message;

        try {

            aNum.sendKeys(a);

            bNum.sendKeys(b);

            button.click();

            // если есть сообщение об ошибке - тест пройден успешно

            message = webDriver.findElement(By.xpath(message_xpath));

            return true; // true - тест пройден успешно
        }
        catch (Exception e)
        {
            return false; // иначе тест провален
        }

        finally {
            webDriver.quit();
        }

    }


    private static boolean successTestExecute(String a, String b) throws Exception {

        double aN = Double.valueOf(a);

        double bN = Double.valueOf(b);

        double cN = aN/bN;

        webDriver = new FirefoxDriver();

        webDriver.get(url);

        WebElement expectation = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(button_xpath)));

        WebElement aNum = webDriver.findElement(By.xpath(a_xpath));

        WebElement bNum = webDriver.findElement(By.xpath(b_xpath));

        WebElement button = webDriver.findElement(By.xpath(button_xpath));

        WebElement result;

        try {

            aNum.sendKeys(a);

            bNum.sendKeys(b);

            button.click();

            Thread.sleep(1000);

            NumberFormat doubleformat = new DecimalFormat("#.####"); // приводим вывод к данному формату

            result = webDriver.findElement(By.xpath(c_xpath));

            String res = result.getAttribute("value");

            if (doubleformat.format(cN).equals(res)) return true; // true - тест пройден успешно
            else return false;

        }
        catch (Exception e)
        {
            return false; // иначе тест провален
        }

        finally {
            webDriver.quit();
        }

    }

    private static void zeroTest() throws Exception { // Тест деления на 0

        if (failureTestExecute("15","0")) System.out.println("Тест деления на 0 пройден успешно!");
        else System.out.println("Тест деления на 0 провален!");

    }

    private static void incorrectTextTest() throws Exception { // Тест на ввод текстовых символов

        if (failureTestExecute("15q","1")) System.out.println("Тест на ввод текстовых символов пройден успешно!");
        else System.out.println("Тест на ввод текстовых символов провален!");

    }

    private static void incorrectSpecTest() throws Exception { // Тест на ввод спецсимволов

        if (failureTestExecute("15","5@")) System.out.println("Тест на ввод спецсимволов пройден успешно!");
        else System.out.println("Тест на ввод спецсимволов провален!");

    }


    private static void successTest() throws Exception { // Тест на корректное вычисление

        if (successTestExecute("15","5")) System.out.println("Тест на корректное вычисление пройден успешно!");
        else System.out.println("Тест на корректное вычисление провален!");

    }

}